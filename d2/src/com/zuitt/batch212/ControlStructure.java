package com.zuitt.batch212;

public class ControlStructure {
    public static void main(String[] args) {

//        Conditional statements

        int num1 = 10;
        int num2 = 20;

//        if statement

        if (num1 > 5)
            System.out.println("num1 is greater than 5");

//        if-else statement
        if (num2 < 100)
            System.out.println("num2 is greater than 100");
        else
            System.out.println("num2 is less than 100");


//        if-else if and else statement
        if (num1 == 5)
            System.out.println("num1 is equal to 5");
        else if (num2 == 20)
            System.out.println("num1 is equal to 20");
        else
            System.out.println("Anything else");

        boolean isHandsome = true;

        if (isHandsome)
            System.out.println("Hello handsome!");
        else
            System.out.println("You're not Handsome");

//        Short Circuiting
//        $ and | (Logical), always evaluate both sides
//        && and || (Short Circuit),
//        false && ... it is not necessary to know what the right hand side because the result can only be false regardless of the value there.

        int x = 15;
        int y = 0;

//        System.out.println(x/y == 0);

        if (y > 5 && x/y == 0)
            System.out.println("Result is:"+ x/y);
        else
            System.out.println("The condition has short circuited");


//        Switch statement

        int directionValue = 4;

        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("East");
                break;
            case 3:
                System.out.println("West");
                break;
            case 4:
                System.out.println("South");
                break;
            default:
                System.out.println("Invalid!");
                break;
        }





    }
}
