package com.zuitt.batch212;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class WDC043_S2_A2 {
    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

//        Array

        int[] array = new int[5];

        array[0] = 5;
        array[1] = 2;
        array[2] = 4;
        array[3] = 1;
        array[4] = 3;

        System.out.println("The first primary number is:" + " " + array[0]);

//        Array lists

        String stringArray[] = new String[3];

        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Mina";

        System.out.println("My friends are:"+ " " + Arrays.toString(stringArray));

//        Hashmap

        HashMap<String, Integer> mouthHygiene = new HashMap<>();

        mouthHygiene.put("ToothBrush " , 500);
        mouthHygiene.put("toothPaste " , 1000);

        System.out.println("Our current inventory consists of: " + " " + mouthHygiene);



    }
}
