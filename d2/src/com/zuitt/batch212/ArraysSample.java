package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {

    public static void main(String[] args) {

//        [Arrays] - fixed/limited collection of data.

//        2^31 = 2, 147, 483, 648 elements

//        Declaration
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

//        other syntax used to declare arrays
        int intSample[] = new int[2];

        intSample[0] = 50;
        intSample[1] = 100;

        System.out.println(intSample[0]);

//        String array
        String stringArray[] = new String[3];

        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Mina";

//        Declaration with initialization

        int[] intArray2 = {100, 200, 300, 400, 500};

//        System.out.println(intArray2);

        System.out.println(Arrays.toString(intArray2));
//        ("[]")
        System.out.println(Arrays.toString(stringArray));

//        method used in arrays
//        sort()
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

//        Binary search

        String searchTerm = "Kelly";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);
//        binarySearch searches the specified array of the given data type for the Arrays.sort() method prior to making this call. If it is not sorted, the results are undefined.

//        ***** Multidimensional Array

        String[][] classroom = new String[3][3];

//        first row
        classroom[0][0] = "Dahyung";
        classroom[0][1] = "Chungchang";
        classroom[0][2] = "Nyek";
//        second row
        classroom[1][0] = "Mark";
        classroom[1][1] = "Jack";
        classroom[1][2] = "Tekla";
//        third row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yeah";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));

//        **** Operators *****

//        1. Arithmetic -> +, -, *, /, %
//        2. Comparison -> >, <, >=, <=, ==, !=
//        3. Assignment -> =, +=
//        4. Logical -> &&, ||, !

//        ArrayLists - resizable arrays, wherein elements can be added or removed whenever it is needed.

//        Declaration
        ArrayList<String> students = new ArrayList<>();

        students.add("John");
        students.add("paul");
        System.out.println(students);

//        Access elements to an ArrayList
        System.out.println(students.get(0));

//        Update/changing elements
        students.set(1, "George");
        System.out.println(students);

//        Removing elements
        students.remove(1);
        System.out.println(students);

//        Removing all elements in an ArrayList using the clear()
        students.clear();
        System.out.println(students);

//        Getting the number of elements in an ArrayList using size()
//        get the length of an array list
        System.out.println(students.size());

//        We can also initialize a values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));

        System.out.println(employees);


//        Hashmaps

        HashMap<String, String> employeeRole = new HashMap<>();

//        Adding a fields

        employeeRole.put("Captain", "Greg");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

//        Retrieving field value
        System.out.println(employeeRole.get("Captain"));

//        Removing element
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

//        Retrieving hashmap keys/fields
        System.out.println(employeeRole.keySet());

//        with Integers as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("english", 89);
        grades.put("math", 93);
        System.out.println(grades);

//        Hashmap with Array Lists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradeListsA = new ArrayList<>(Arrays.asList(75, 80, 90));

        ArrayList<Integer> gradeListsB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradeListsA);
        subjectGrades.put("Jane", gradeListsB);

        System.out.println(subjectGrades);






    }


}
